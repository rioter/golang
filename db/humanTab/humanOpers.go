package humanTab

import "database/sql"

type HumanMethods interface {
	InsertHumanTab(*sql.DB, string, int) error
	Filter(*sql.DB, string, string) error
}
