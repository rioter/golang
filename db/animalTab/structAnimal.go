package animalTab

import (
	"database/sql"
	"fmt"
	sq "github.com/Masterminds/squirrel"
)

type NewAnimal struct {
}

var nA = NewAnimal{}

func (NewAnimal) InsertAnimalTab(db *sql.DB, val1 string, val2 int) error {
	//_, err := nA.db.Exec("INSERT INTO animal (anType,age) VALUES (?,?)", val1, val2)
	sql, args, err := sq.Insert("animal").Columns("anType", "age").Values(val1, val2).ToSql()
	_, err = db.Exec(sql, args[0], args[1])
	return err
}
func (NewAnimal) Filter(db *sql.DB, columname string, val string) error {
	sql, args, err := sq.Select("anType", "age").From("animal").Where(sq.Eq{columname: val}).ToSql()
	rows, err := db.Query(sql, args[0])
	names := make([]string, 0)
	defer rows.Close()
	for rows.Next() {
		var val2 string
		err = rows.Scan(&val, &val2)
		names = append(names, val, val2)
	}
	fmt.Print(names)
	return err
}
