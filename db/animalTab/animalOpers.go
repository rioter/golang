package animalTab

import "database/sql"

type AnimalMethods interface {
	InsertAnimalTab(*sql.DB, string, int) error
	Filter(*sql.DB, string, string) error
}
