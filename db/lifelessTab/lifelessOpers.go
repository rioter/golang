package lifelessTab

import "database/sql"

type SubjectMethods interface {
	InsertSubjectTab(*sql.DB, string, int) error
	Filter(*sql.DB, string, string) error
}
