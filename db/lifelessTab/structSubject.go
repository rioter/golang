package lifelessTab

import (
	"database/sql"
	"fmt"
	sq "github.com/Masterminds/squirrel"
)

type NewSubject struct {
}

var nS = NewSubject{}

func (NewSubject) InsertSubjectTab(db *sql.DB, val1 string, val2 int) error {
	sql, args, err := sq.Insert("subjects").Columns("sub", "amount").Values(val1, val2).ToSql()
	_, err = db.Exec(sql, args[0], args[1])
	return err
}
func (NewSubject) Filter(db *sql.DB, columname string, val string) error {
	sql, args, err := sq.Select("sub", "amount").From("subjects").Where(sq.Eq{columname: val}).ToSql()
	rows, err := db.Query(sql, args[0])
	names := make([]string, 0)
	defer rows.Close()
	for rows.Next() {
		var val2 string
		err = rows.Scan(&val, &val2)
		names = append(names, val, val2)
	}
	fmt.Print(names)
	return err
}
