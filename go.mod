module awesomeProject2

go 1.15

require (
	github.com/Masterminds/squirrel v1.4.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gobuffalo/packr/v2 v2.8.0
	github.com/miekg/pkcs11 v1.0.3 // indirect
	github.com/tkanos/gonfig v0.0.0-20181112185242-896f3d81fadf
	github.com/urfave/cli v1.22.4 // indirect
	github.com/urfave/cli/v2 v2.2.0
)
