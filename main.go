package main

import (
	"awesomeProject2/funcs"
	_ "github.com/go-sql-driver/mysql"
	"github.com/urfave/cli/v2"
	"log"
	"os"
)

func main() {
	db, err := funcs.Connection()
	app := cli.NewApp()
	app.Commands = cli.Commands{
		{
			Name: "migrate",
			Subcommands: cli.Commands{
				{
					Name: "down",
					Action: func(c *cli.Context) error {
						err = funcs.MigrateDown(db)
						return err
					},
				},
				{
					Name: "up",
					Action: func(c *cli.Context) error {
						err = funcs.MigrateUp(db)
						return err
					},
				},
			},
		},
		{
			Name: "run",
			Action: func(c *cli.Context) error {
				return funcs.Run(db)
			},
		},
		{
			Name: "filter",
			Action: func(c *cli.Context) error {
				return funcs.FilterTab(db, os.Args[2], os.Args[3], os.Args[4])
			},
		},
	}
	err = app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
	err = db.Close()
}
