package funcs

import (
	"awesomeProject2/db/animalTab"
	"awesomeProject2/db/humanTab"
	"awesomeProject2/db/lifelessTab"
	"database/sql"
	"github.com/gobuffalo/packr/v2"
	"strings"
)

type Fields struct {
	X string
	Y int
}

var human humanTab.HumanMethods = humanTab.NewHuman{}
var animal animalTab.AnimalMethods = animalTab.NewAnimal{}
var subject lifelessTab.SubjectMethods = lifelessTab.NewSubject{}
var box = packr.New("box", "./req")

func Connection() (*sql.DB, error) {
	settings := GetConfig()
	cfg := settings.DB_USERNAME + ":" + settings.DB_PASSWORD + "@(" + settings.DB_HOST + ":" + settings.DB_PORT + ")/" + settings.DB_NAME
	//db, err := sql.Open("mysql", "root:new-password@(127.0.0.1:3306)/testdb")
	db, err := sql.Open("mysql", cfg)
	return db, err
}
func Run(db *sql.DB) error {
	var err error
	fHuman := []Fields{
		{
			X: "Vanya",
			Y: 30,
		},
		{
			X: "Sanya",
			Y: 21,
		},
		{
			X: "Tanya",
			Y: 20,
		},
	}
	fAninmal := []Fields{
		{
			X: "dog",
			Y: 4,
		},
		{
			X: "cat",
			Y: 5,
		},
		{
			X: "bird",
			Y: 2,
		},
	}
	fSub := []Fields{
		{
			X: "book",
			Y: 40,
		},
		{
			X: "pen",
			Y: 100,
		},
		{
			X: "cup",
			Y: 20,
		},
	}
	for _, fields := range fHuman {
		err = human.InsertHumanTab(db, fields.X, fields.Y)
		if err != nil {
			return err
		}
	}
	for _, fields := range fAninmal {
		err = animal.InsertAnimalTab(db, fields.X, fields.Y)
		if err != nil {
			return err
		}
	}
	for _, fields := range fSub {
		err = subject.InsertSubjectTab(db, fields.X, fields.Y)
		if err != nil {
			return err
		}
	}
	return nil
}

func MigrateUp(db *sql.DB) error {
	rq, err := box.FindString("migration.txt")
	up := strings.Split(rq, "!")
	for i := 0; i <= 2; i++ {
		_, err = db.Exec(up[i])
	}
	return err
}

func MigrateDown(db *sql.DB) error {
	rq, err := box.FindString("migration.txt")
	up := strings.Split(rq, "!")
	for i := 3; i <= 5; i++ {
		_, err = db.Exec(up[i])
	}
	return err
}
func FilterTab(db *sql.DB, table, columnname, value string) error {
	var err error
	switch table {
	case "human":
		err = human.Filter(db, columnname, value)
	case "animal":
		err = animal.Filter(db, columnname, value)
	case "subjects":
		err = subject.Filter(db, columnname, value)
	}
	return err
}
