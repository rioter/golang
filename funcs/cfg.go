package funcs

import (
	"github.com/tkanos/gonfig"
)

type Configuration struct {
	DB_USERNAME string
	DB_PASSWORD string
	DB_PORT     string
	DB_HOST     string
	DB_NAME     string
}

func GetConfig() Configuration {
	configuration := Configuration{}
	fileName := "funcs/config.json"
	gonfig.GetConf(fileName, &configuration)
	return configuration
}
